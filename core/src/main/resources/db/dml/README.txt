All scripts that populate the schema with data, establish relationships,
or manipulate the data without changing table structures should go here.