package com.thebuffalogroup.softlyticslight.repo;


import java.util.List;

import com.thebuffalogroup.softlyticslight.entity.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {
  List<Contact> findByPhone(String phone);

  List<Contact> findByStateId(Long id);
}
