package com.thebuffalogroup.softlyticslight.repo;
import com.thebuffalogroup.softlyticslight.entity.Process;
import org.springframework.data.repository.CrudRepository;

public interface ProcessRepository extends CrudRepository<Process, Long> {}
