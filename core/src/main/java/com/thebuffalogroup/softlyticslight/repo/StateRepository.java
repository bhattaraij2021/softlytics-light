package com.thebuffalogroup.softlyticslight.repo;
import com.thebuffalogroup.softlyticslight.entity.State;
import org.springframework.data.repository.CrudRepository;

public interface StateRepository extends CrudRepository<State, Long> {
  public State getStateByName(String name);
}
