package com.thebuffalogroup.softlyticslight.repo;
import com.thebuffalogroup.softlyticslight.entity.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long> {}
