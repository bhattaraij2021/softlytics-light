package com.thebuffalogroup.softlyticslight.repo;
import com.thebuffalogroup.softlyticslight.entity.Tool;
import org.springframework.data.repository.CrudRepository;

public interface ToolRepository extends CrudRepository<Tool, Long> {}
