package com.thebuffalogroup.softlyticslight.repo;
import java.util.List;

import com.thebuffalogroup.softlyticslight.entity.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
  List<Person> findByLastName(String lastName);
}
