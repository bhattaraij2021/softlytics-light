package com.thebuffalogroup.softlyticslight.repo;
import java.util.List;

import com.thebuffalogroup.softlyticslight.entity.RoleAssignment;
import com.thebuffalogroup.softlyticslight.entity.RoleAssignmentIdentity;
import org.springframework.data.repository.CrudRepository;

public interface RoleAssignmentRepository
    extends CrudRepository<RoleAssignment, RoleAssignmentIdentity> {

  List<RoleAssignment> findByPersonId(Long personId);

  List<RoleAssignment> findByTeamId(Long teamId);

  List<RoleAssignment> findByRoleId(Long roleId);
}
