package com.thebuffalogroup.softlyticslight.repo;
import com.thebuffalogroup.softlyticslight.entity.Dependency;
import org.springframework.data.repository.CrudRepository;

public interface DependencyRepository extends CrudRepository<Dependency, Long> {}
