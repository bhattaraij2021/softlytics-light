package com.thebuffalogroup.softlyticslight.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RoleAssignmentIdentity implements Serializable {

  @Column(name = "person_id")
  public Long personId;

  @Column(name = "team_id")
  public Long teamId;

  @Column(name = "role_id")
  public Long roleId;

  public Long getPersonId() {
    return personId;
  }

  public void setPersonId(Long personId) {
    this.personId = personId;
  }

  public Long getTeamId() {
    return teamId;
  }

  public void setTeamId(Long teamId) {
    this.teamId = teamId;
  }

  public Long getRoleId() {
    return roleId;
  }

  public void setRoleId(Long roleId) {
    this.roleId = roleId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    RoleAssignmentIdentity that = (RoleAssignmentIdentity) o;

    if (personId != null ? !personId.equals(that.personId) : that.personId != null) return false;
    if (teamId != null ? !teamId.equals(that.teamId) : that.teamId != null) return false;
    return roleId != null ? roleId.equals(that.roleId) : that.roleId == null;
  }

  @Override
  public int hashCode() {
    int result = personId != null ? personId.hashCode() : 0;
    result = 31 * result + (teamId != null ? teamId.hashCode() : 0);
    result = 31 * result + (roleId != null ? roleId.hashCode() : 0);
    return result;
  }
}
