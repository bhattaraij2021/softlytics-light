package com.thebuffalogroup.softlyticslight.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "program")
public class Program {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "comment")
  private String comment;

  @Column(name = "date_start")
  private Date startDate;

  @Column(name = "date_end")
  private Date endDate;

  @Column(name = "budget")
  private BigDecimal budget;
}
