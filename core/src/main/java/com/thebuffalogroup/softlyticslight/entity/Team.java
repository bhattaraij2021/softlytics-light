package com.thebuffalogroup.softlyticslight.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "team")
public class Team {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "team_id", referencedColumnName = "id")
  private List<RoleAssignment> assignments;

  @Column(name = "name")
  private String name;

  @Column(name = "comment")
  private String comment;

  public List<RoleAssignment> getAssignments() {
    return assignments;
  }

  @ManyToOne
  @JoinColumn(name = "project_id")
  private Project project;

  public Long getId() {
    return id;
  }

  public void setAssignments(List<RoleAssignment> assignments) {
    this.assignments = assignments;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  public Team(String name) {
    this.name = name;
  }

  public Team() {}

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Team team = (Team) o;

    if (!Objects.equals(name, team.name)) return false;
    if (!Objects.equals(comment, team.comment)) return false;
    return Objects.equals(project, team.project);
  }

  @Override
  public int hashCode() {
    int result = name != null ? name.hashCode() : 0;
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    result = 31 * result + (project != null ? project.hashCode() : 0);
    return result;
  }
}
