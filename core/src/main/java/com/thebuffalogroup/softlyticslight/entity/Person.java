package com.thebuffalogroup.softlyticslight.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "person")
public class Person {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  public Long getId() {
    return id;
  }

  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "person_id", referencedColumnName = "id")
  private List<RoleAssignment> assignments;

  @Column(name = "f_name")
  private String firstName;

  @Column(name = "m_name")
  private String middleName;

  @Column(name = "l_name")
  private String lastName;

  @ManyToOne
  @JoinColumn(name = "contact_id")
  private Contact contact;

  public Person(String firstName, String middleName, String lastName) {
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
  }

  public List<RoleAssignment> getAssignments() {
    return assignments;
  }

  public void setAssignments(List<RoleAssignment> assignments) {
    this.assignments = assignments;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Contact getContact() {
    return contact;
  }

  public void setContact(Contact contact) {
    this.contact = contact;
  }

  public Person() {}

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Person person = (Person) o;

    if (!Objects.equals(firstName, person.firstName)) return false;
    if (!Objects.equals(middleName, person.middleName)) return false;
    if (!Objects.equals(lastName, person.lastName)) return false;

    return Objects.equals(contact, person.contact);
  }

  @Override
  public int hashCode() {
    int result = assignments != null ? assignments.hashCode() : 0;
    result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
    result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
    result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
    result = 31 * result + (contact != null ? contact.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Person{"
        + "id="
        + id
        + ", assignments="
        + (assignments != null ? assignments : "[null]")
        + ", firstName='"
        + firstName
        + '\''
        + ", middleName='"
        + middleName
        + '\''
        + ", lastName='"
        + lastName
        + '\''
        + ", contact="
        + contact
        + '}';
  }
}
