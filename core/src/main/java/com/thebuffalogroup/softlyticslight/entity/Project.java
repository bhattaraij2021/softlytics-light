package com.thebuffalogroup.softlyticslight.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "comment")
  private String comment;

  @Column(name = "date_start")
  private Date startDate;

  @Column(name = "date_end")
  private Date endDate;

  @Column(name = "total_cost")
  private BigDecimal totalCost;

  @ManyToMany
  @JoinTable(
      name = "xref_dependency_project",
      joinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "dependency_id", referencedColumnName = "id"))
  private List<Dependency> dependencies;

  @ManyToOne
  @JoinColumn(name = "program_id")
  private Program program;
}
