package com.thebuffalogroup.softlyticslight.entity;

import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "xref_team_person_role")
public class RoleAssignment {

  @EmbeddedId private RoleAssignmentIdentity id;

  @ManyToOne
  @MapsId("personId")
  @JoinColumn(name = "person_id")
  Person person;

  @ManyToOne
  @MapsId("teamId")
  @JoinColumn(name = "team_id")
  Team team;

  @ManyToOne
  @MapsId("roleId")
  @JoinColumn(name = "list_role_id")
  Role role;

  @Column(name = "date_created")
  Date createdDate;

  @Column(name = "date_start")
  Date startDate;

  @Column(name = "date_end")
  Date endDate;

  @Column(name = "is_active")
  Boolean isActive;

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public Team getTeam() {
    return team;
  }

  public void setTeam(Team team) {
    this.team = team;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public Boolean isActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }

  public RoleAssignment() {
    super();
    // Must have this, otherwise we get "PropertyAccessException: Could not set field value by
    // reflection"
    this.id = new RoleAssignmentIdentity();
  }

  @PrePersist
  public void populateCreatedDate() {
    this.createdDate = new Date();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    RoleAssignment that = (RoleAssignment) o;

    if (isActive != that.isActive) return false;
    if (!Objects.equals(person, that.person)) return false;
    if (!Objects.equals(team, that.team)) return false;
    if (!Objects.equals(role, that.role)) return false;
    if (!Objects.equals(createdDate, that.createdDate)) return false;
    if (!Objects.equals(startDate, that.startDate)) return false;
    return Objects.equals(endDate, that.endDate);
  }

  @Override
  public int hashCode() {
    int result = person != null ? person.hashCode() : 0;
    result = 31 * result + (team != null ? team.hashCode() : 0);
    result = 31 * result + (role != null ? role.hashCode() : 0);
    result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
    result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
    result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
    result = 31 * result + (isActive ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "RoleAssignment{"
        + "id="
        + id
        + ", person="
        + person.getFirstName()
        + " "
        + person.getLastName()
        + ", team="
        + team.getName()
        + ", role="
        + role.getName()
        + ", createdDate="
        + createdDate
        + ", startDate="
        + startDate
        + ", endDate="
        + endDate
        + ", isActive="
        + isActive
        + '\''
        + '}';
  }
}
