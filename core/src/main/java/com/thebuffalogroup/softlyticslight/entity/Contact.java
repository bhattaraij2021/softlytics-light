package com.thebuffalogroup.softlyticslight.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contact")
public class Contact {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "address_ln1")
  private String addressLine1;

  @Column(name = "address_ln2")
  private String addressLine2;

  @Column(name = "city")
  private String city;

  @ManyToOne
  @JoinColumn(name = "region_id")
  private State state;

  @Column(name = "phone")
  private String phone;

  @Column(name = "email")
  private String email;

  @Column(name = "preferred_contact")
  private String preferredContact;

  @Column(name = "comment")
  private String comment;

  public Contact() {}

  public Contact(
      String addressLine1,
      String addressLine2,
      String city,
      State state,
      String phone,
      String email,
      String preferredContact,
      String comment) {
    this.addressLine1 = addressLine1;
    this.addressLine2 = addressLine2;
    this.city = city;
    this.state = state;
    this.phone = phone;
    this.email = email;
    this.preferredContact = preferredContact;
    this.comment = comment;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPreferredContact() {
    return preferredContact;
  }

  public void setPreferredContact(String preferredContact) {
    this.preferredContact = preferredContact;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Contact contact = (Contact) o;

    if (!id.equals(contact.id)) return false;
    if (addressLine1 != null
        ? !addressLine1.equals(contact.addressLine1)
        : contact.addressLine1 != null) return false;
    if (addressLine2 != null
        ? !addressLine2.equals(contact.addressLine2)
        : contact.addressLine2 != null) return false;
    if (city != null ? !city.equals(contact.city) : contact.city != null) return false;
    if (state != null ? !state.equals(contact.state) : contact.state != null) return false;
    if (phone != null ? !phone.equals(contact.phone) : contact.phone != null) return false;
    if (email != null ? !email.equals(contact.email) : contact.email != null) return false;
    if (preferredContact != null
        ? !preferredContact.equals(contact.preferredContact)
        : contact.preferredContact != null) return false;
    return comment != null ? comment.equals(contact.comment) : contact.comment == null;
  }

  @Override
  public int hashCode() {
    int result = id.hashCode();
    result = 31 * result + (addressLine1 != null ? addressLine1.hashCode() : 0);
    result = 31 * result + (addressLine2 != null ? addressLine2.hashCode() : 0);
    result = 31 * result + (city != null ? city.hashCode() : 0);
    result = 31 * result + (state != null ? state.hashCode() : 0);
    result = 31 * result + (phone != null ? phone.hashCode() : 0);
    result = 31 * result + (email != null ? email.hashCode() : 0);
    result = 31 * result + (preferredContact != null ? preferredContact.hashCode() : 0);
    result = 31 * result + (comment != null ? comment.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Contact{"
        + "addressLine1='"
        + addressLine1
        + '\''
        + ", addressLine2='"
        + addressLine2
        + '\''
        + ", city='"
        + city
        + '\''
        + ", state="
        + state
        + ", phone='"
        + phone
        + '\''
        + ", email='"
        + email
        + '\''
        + ", preferredContact='"
        + preferredContact
        + '\''
        + ", comment='"
        + comment
        + '\''
        + '}';
  }
}
