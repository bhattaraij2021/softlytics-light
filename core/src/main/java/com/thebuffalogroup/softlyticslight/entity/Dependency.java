package com.thebuffalogroup.softlyticslight.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/*
   The purpose of this entity is not obvious; comments, please?
*/
@Entity
@Table(name = "dependency")
public class Dependency {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name")
  private String name;

  @Column(name = "comment")
  private String comment;

  @Column(name = "type")
  private Long type;

  @Column(name = "cost")
  private BigDecimal cost;

  @Column(name = "date_start")
  private Date startDate;

  @ManyToMany
  @JoinTable(
      name = "xref_dependency_project",
      joinColumns = @JoinColumn(name = "dependency_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"))
  private List<Project> projects;
}
