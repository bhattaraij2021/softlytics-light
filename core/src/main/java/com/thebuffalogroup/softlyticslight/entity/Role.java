package com.thebuffalogroup.softlyticslight.entity;

import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "list_role")
public class Role {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "list_role_id", referencedColumnName = "id")
  private List<RoleAssignment> assignments;

  @Column(name = "name")
  private String name;

  public Long getId() {
    return id;
  }

  public List<RoleAssignment> getAssignments() {
    return assignments;
  }

  public void setAssignments(List<RoleAssignment> assignments) {
    this.assignments = assignments;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Role(String name) {
    this.name = name;
  }

  public Role() {}

  @Override
  public String toString() {
    return "Role {" + "id=" + id + ", name='" + name + '\'' + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Role role = (Role) o;

    return Objects.equals(name, role.name);
  }

  @Override
  public int hashCode() {
    return name != null ? name.hashCode() : 0;
  }
}
