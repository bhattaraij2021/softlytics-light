package com.thebuffalogroup.softlyticslight.api;

import com.thebuffalogroup.softlyticslight.entity.SampleContact;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RestController
@RequestMapping("/api")
public class SampleContactResource {
    ConcurrentMap<String, SampleContact> contacts = new ConcurrentHashMap<>();

    @GetMapping("/{id}")
    public SampleContact getContact(@PathVariable String id){
        return contacts.get(id);
    }

    @GetMapping("/")
    public List<SampleContact> getAllContacts(){
        return new ArrayList<SampleContact>(contacts.values());
    }

    @PostMapping("/")
    public SampleContact addContact(@RequestBody SampleContact sampleContact){
        contacts.put(sampleContact.getId(), sampleContact);
        return sampleContact;
    }
}
